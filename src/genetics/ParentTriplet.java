package genetics;

public class ParentTriplet {
	
	public Individual i1 = null;
	public Individual i2 = null;
	public Individual i3 = null;
	
	public ParentTriplet() {
	}
	
	public boolean filled() {
		if (i1 == null || i2 == null || i3 == null)
			return false;
		else
			return true;
	}
	
	public void add(Individual i) {
		if (i1 == null)
			i1 = i;
		else if (i2 == null)
			i2 = i;
		else if (i3 == null)
			i3 = i;
		else
			System.out.println("***WARNING*** Trying to add an Individual to an already filled ParentTriplet");
		
	}
	
	public int count() {
		int c = 0;
		if (i1 != null)
			c++;
		if (i2 != null)
			c++;
		if (i3 != null)
			c++;
		return c;
	}
}
