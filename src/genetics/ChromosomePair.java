package genetics;

public class ChromosomePair {

	Chromosome c1 = null;
	Chromosome c2 = null;
	
	public ChromosomePair() {
		
	}
	
	public boolean filled() {
		if (c1 == null || c2 == null)
			return false;
		else
			return true;
	}
	
	public void add(Chromosome c) {
		if (c1 == null)
			c1 = c;
		else if (c2 == null)
			c2 = c;
		else 
			System.out.println("***WARNING*** Trying to add chromsomes to a filled ChromosomePair");
	}
}
