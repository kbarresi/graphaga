package genetics;

public class Fitness {

	public double crossing = 0;
	public double force = 0;
	public double alpha = 0;
	
	public double score = -1;
	
	
	public double rawCrossing = 0;
	public double rawForce = 0;
	public double rawAlpha = 0;
	public double rawScore = 0;
}
