package genetics;

import java.awt.Point;

import kbarrersi.Vertex;

import org.jgrapht.UndirectedGraph;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.SimpleGraph;

public class Individual {
	private Chromosome c1 = null;
	private Chromosome c2 = null;
	
	private  UndirectedGraph<Vertex, DefaultEdge> m_phenotype = null;
	private Fitness m_fitness = new Fitness();
	
	public boolean mutated = false;
	private boolean m_selectedForReproduction = false;
	private boolean m_elite = false;
	private boolean m_markedForDeath = false;
	public Individual() {
	}
	
	
	
	/*
	 * EXTERNAL UPDATE METHODS
	 */
	public void calculatePhenotype() {
		if (m_phenotype != null)
			m_phenotype = null;
		
		m_phenotype = new SimpleGraph<Vertex, DefaultEdge>(DefaultEdge.class);
		
		Object[] chr1Edges = c1.graph().edgeSet().toArray();
		Object[] chr2Edges = c2.graph().edgeSet().toArray();

		for (int i = 0; i < chr1Edges.length; i++) {
			DefaultEdge e1 = (DefaultEdge) chr1Edges[i];
			DefaultEdge e2 = (DefaultEdge) chr2Edges[i];
			
			Vertex ch1Source = c1.graph().getEdgeSource(e1);
			Vertex ch1Target = c1.graph().getEdgeTarget(e1);
			Vertex ch2Source = c2.graph().getEdgeSource(e2);
			Vertex ch2Target = c2.graph().getEdgeTarget(e2);
			
			if (ch1Source.id != ch2Source.id)
				System.out.println("WARNING***\tGraph verticies in Chr1 and Chr2 don't match up: " + ch1Source.id + " : " + ch2Source.id);
			if (ch1Target.id != ch2Target.id)
				System.out.println("WARNING***\tGraph verticies in Chr1 and Chr2 don't match up: " + ch1Target.id + " : " + ch2Target.id);
				
			Point sourceAvg = new Point();
			sourceAvg.x = (ch1Source.position.x + ch2Source.position.x) / 2;
			sourceAvg.y = (ch1Source.position.y + ch2Source.position.y) / 2;
			
			Point targetAvg = new Point();
			targetAvg.x = (ch1Target.position.x + ch2Target.position.x) / 2;
			targetAvg.y = (ch1Target.position.y + ch2Target.position.y) / 2;
			
			
			Vertex newSource = Population.vertexById(m_phenotype, ch1Source.id);	//ch1Source.id == ch2Source.id
			Vertex newTarget = Population.vertexById(m_phenotype, ch1Target.id);	//ch1Target.id == ch2Target.id
			
			if (newSource.id == -1) {
				newSource.id = ch1Source.id;
				newSource.position = sourceAvg;
				m_phenotype.addVertex(newSource);
			}
			if (newTarget.id == -1) {
				newTarget.id = ch1Target.id;
				newTarget.position = targetAvg;
				m_phenotype.addVertex(newTarget);
			}
			m_phenotype.addEdge(newSource, newTarget);
		}
//		System.out.print("Done");
	}
			

/*
 * GETTERS AND SETTERS
 */
	public Chromosome chromosomeTwo() {	return c2; }
	public Chromosome chromosomeOne() { return c1; }
	public void setChromosomeTwo(Chromosome c2) { this.c2 = c2; }
	public void setChromosomeOne(Chromosome c1) { this.c1 = c1;	}



	public Fitness fitness() { return m_fitness; }
	public void setFitness(Fitness m_fitness) { this.m_fitness = m_fitness; }

	public UndirectedGraph<Vertex, DefaultEdge> phenotype() { return m_phenotype; }
	public void setPhenotype(UndirectedGraph<Vertex, DefaultEdge> p) { m_phenotype = p; }



	public boolean reproduction() { return m_selectedForReproduction; }
	public void setReproduction(boolean select) { m_selectedForReproduction = select; }

	public boolean elite() { return m_elite; }
	public void setElite(boolean select) { m_elite = select; };
	
	public boolean markedForDeath() { return m_markedForDeath; }
	public void setMarkedForDeath(boolean marked) { m_markedForDeath = marked; };
}
