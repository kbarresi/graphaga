package genetics;

import math.geom2d.Point2D;
import math.geom2d.line.Line2D;

import java.awt.Point;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Random;

import kbarrersi.Main;
import kbarrersi.Vertex;

import org.jgrapht.Graphs;
import org.jgrapht.UndirectedGraph;
import org.jgrapht.alg.FloydWarshallShortestPaths;
import org.jgrapht.alg.StoerWagnerMinimumCut;
import org.jgrapht.graph.AsWeightedGraph;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.DefaultWeightedEdge;
import org.jgrapht.graph.SimpleGraph;

import util.IndividualFitnessComparator;

public class Population {
	public static final boolean DYNAMIC_MUTATION = true;
	
	
	public static final int MAX_GENERATION = 1000;
	public static final int POPULATION_SIZE = 200;
	public static final int VERTEX_WIDTH = 25;
	public static final int GRID_SIZE = 50;
	
	public static final double MUTATION_FLOOR = 0.01;
	public static final double MUTATION_CEIL = 0.1;
	
	public static final boolean PHASE_LABELS = false;
	public static final boolean PRINT_STATS = false;
	public static final boolean PRINT_AVG_FITNESS = true;
	public static final boolean SINGLE_VERTEX_GENES = false;
	
	public static final int FITNESS_HISTORY = 4;	//how many fitnesses are kept in m_fitnessHistory
	Main m_main;

	private UndirectedGraph<Vertex, DefaultEdge> m_graph;
	
	int generation = 1;
	
	private double P_m = 0.01;
	private double P_c = 0.33;
	
	private double[] m_weights = { 0, 1, 0.5};
	
	public static final int WEIGHT_EX = 0;
	public static final int WEIGHT_ENERGY = 1;
	public static final int WEIGHT_ALPHA = 2;
	
	int m_alphaIdeal = 1;
	
	public enum Mutation {
	    SHIFT, PIVOT, EDGE 
	}
	
	private ArrayList<Fitness> m_fitnessHistory = new ArrayList<Fitness>();;
	
	private ArrayList<Individual> m_individuals = new ArrayList<Individual>();
	private ArrayList<Gene> m_genes = new ArrayList<Gene>();
	
	Random m_random;
	public Population(UndirectedGraph<Vertex, DefaultEdge> pGraph, Main m) {
		m_main = m;
		m_graph = pGraph;
		m_random = new Random();
		
		m_alphaIdeal = m_graph.vertexSet().size();
	}

	public UndirectedGraph<Vertex, DefaultEdge> run() {
		initialization();
		while (generation < MAX_GENERATION) {
			evaluation();
			selection();
			reproduction();
			mutation();
			adaptation();
		}
		termination();

		return m_graph;
	}
	
	
	private void initialization() {
		if (PHASE_LABELS)
			System.out.println("INITIALIZATION");
		int bound = (int) (Math.sqrt(m_graph.vertexSet().size()) * 2 * VERTEX_WIDTH);
		for (int i = 1; i <= POPULATION_SIZE; i++) {
			Individual individual = new Individual();

			UndirectedGraph<Vertex, DefaultEdge> ch1Graph = Main.deepGraphClone(m_graph);
			UndirectedGraph<Vertex, DefaultEdge> ch2Graph = Main.deepGraphClone(m_graph);
			
			Object[] vertexArray1 = ch1Graph.vertexSet().toArray();
			Object[] vertexArray2 = ch2Graph.vertexSet().toArray();
			
			for (int j = 0; j < vertexArray1.length; j++) {
				Vertex v1 = (Vertex) vertexArray1[j];
				Vertex v2 = (Vertex) vertexArray2[j];
				
				v1.position.x = randInt(-bound, bound);
				v1.position.y = randInt(-bound, bound);
				v2.position.x = randInt(-bound, bound);
				v2.position.y = randInt(-bound, bound);
			}
			
			Chromosome ch1 = new Chromosome();
			ch1.setGraph(ch1Graph);
			
			Chromosome ch2 = new Chromosome();
			ch2.setGraph(ch2Graph);
			
			individual.setChromosomeOne(ch1);
			individual.setChromosomeTwo(ch2);
			
			m_individuals.add(individual);
			if (i % 10 == 0)
				if (PHASE_LABELS)
					System.out.println("\t..." + i);

		}
		
		//Now we go on to create the set of Genes;
		calculateGenes(m_graph);
		
		
		m_alphaIdeal = m_graph.vertexSet().size();
		
		if (PHASE_LABELS)
			System.out.println("\tComplete\tCreated: " + m_individuals.size() + "  new individuals\n");
	}
	private void evaluation() {
		if (PHASE_LABELS) {
			System.out.print("\n***GENERATION " + generation + "***\n");
			System.out.print("EVALUATION");
		}
		
		//reset all markers
		for (int i = 0; i < m_individuals.size(); i++) {
			Individual individual = m_individuals.get(i);
			individual.setElite(false);
			individual.setReproduction(false);
			individual.setMarkedForDeath(false);
		}
		
		// go through and set the raw fitness scores, keeping track of max/mins along the way. Then, we 
		//can normalize each individual's fitness
		
		double maxCrossing, minCrossing, maxForce, minForce, maxAlphaDeviation, minAlphaDeviation;
		Individual firstInd = m_individuals.get(0);
		firstInd.calculatePhenotype();
		
		maxCrossing = minCrossing = edgeCrossings(firstInd);
		maxForce = minForce = totalVertexEnergy(firstInd);
		maxAlphaDeviation = minAlphaDeviation = alphaDeviation(firstInd);
		
		Fitness f = new Fitness();
		f.crossing = maxCrossing;
		f.rawCrossing = maxCrossing;
		f.force = minForce;
		f.rawForce = minForce;
		f.alpha = maxAlphaDeviation;
		f.rawAlpha = maxAlphaDeviation;
		f.score = -1;
		f.rawScore = maxCrossing + minForce + maxAlphaDeviation;
		
		firstInd.setFitness(f);
		
		//System.out.println("Fitness:\tCrossings: " + f.crossing + "\tForce: " + f.force + "aDeviation: " + f.alpha);
		
		//Now go through the rest
		for (int i = 1; i < m_individuals.size(); i++) {
			Individual individual = m_individuals.get(i);
			individual.calculatePhenotype();
			
			Fitness fitness = individual.fitness();
			
			double crossing = edgeCrossings(individual);
			double force = totalVertexEnergy(individual);
			double aDeviation = alphaDeviation(individual);
			
			//System.out.println("Fitness:\tCrossings: " + crossing + "\tForce: " + force + "aDeviation: " + aDeviation);
			if (crossing > maxCrossing)
				maxCrossing = crossing;
			else if (crossing < minCrossing)
				minCrossing = crossing;
			
			if (force > maxForce)
				maxForce = force;
			else if (force < minForce)
				minForce = force;
			
			if (aDeviation > maxAlphaDeviation)
				maxAlphaDeviation = aDeviation;
			else if (aDeviation < minAlphaDeviation)
				minAlphaDeviation = aDeviation;
			
			fitness.crossing = crossing;
			fitness.rawCrossing = crossing;
			fitness.force = force;
			fitness.rawForce = force;
			fitness.alpha = aDeviation;
			fitness.rawAlpha = aDeviation;
			fitness.score = -1;
			fitness.rawScore = crossing + force + aDeviation;
			
			individual.setFitness(fitness);
		}
		
		//If mins == maxs, then we'll get a divide by 0 error later. check for this now, and adjust if needed
		if (minCrossing == maxCrossing)
			maxCrossing++;
		
		if (minForce == maxForce)
			maxForce++;
		
		if (minAlphaDeviation == maxAlphaDeviation)
			maxAlphaDeviation++;
		
		
		//So now we can go through and scale everyone's fitness aspects!
		for (int i = 0; i < m_individuals.size(); i++) {
			Individual individual = m_individuals.get(i);
			
			Fitness fitness = individual.fitness();
			//scale: Normalized(e_i) = (e_i - e_min) / (e_max - e_min);
			double crossingScore = (fitness.crossing - minCrossing) / (maxCrossing - minCrossing);				// [0, 1]
			crossingScore = (double)(1) - crossingScore;	//lower crossing score is better
			
			double forceScore = (fitness.force - minForce) / (maxForce - minForce);								// [0, 1]
			forceScore = (double)(1) - forceScore;
			
			double alphaScore = (fitness.alpha - minAlphaDeviation) / (maxAlphaDeviation - minAlphaDeviation);	// [0, 1]
			alphaScore = (double)(1) - alphaScore;
			
			crossingScore *= m_weights[WEIGHT_EX];	//include the crossing weight
			forceScore *= m_weights[WEIGHT_ENERGY];		//include force weight
			alphaScore *= m_weights[WEIGHT_ALPHA];	//include alpha weight;
			
			double cumulativeScore = crossingScore + forceScore + alphaScore;	// [0, 3]
			cumulativeScore /= (m_weights[WEIGHT_EX] + m_weights[WEIGHT_ENERGY] + m_weights[WEIGHT_ALPHA]);
			
			//Update the fitness with our weighted + normalized scores
			fitness.crossing = crossingScore;
			fitness.force = forceScore;
			fitness.alpha = alphaScore;
			fitness.score = cumulativeScore;
			
			//And apply it
			individual.setFitness(fitness);
		}
		
		//Sort the population in terms of fitness
		Collections.sort(m_individuals, new IndividualFitnessComparator());
		
		/*
		for (int i = 0; i < m_individuals.size(); i++)
			if (i % 10 == 0)
				System.out.println("Fitness: Invidivual(" + i + ")\t " + m_individuals.get(i).fitness().score);
		*/
		//The fitnesses have been calculated, and the population individuals are sorted ascending in terms
		//of their fitness scores
		
		if (PHASE_LABELS)
			System.out.print("\t...Complete\n");
	}
	private void selection() {
		if (PHASE_LABELS)
			System.out.print("SELECTION");
		
		//first, go through and mark everyone non-elite, non-reproducing, and not marked for death...yet ;)
		for (int i = 0; i < m_individuals.size(); i++) {
			Individual ind = m_individuals.get(i);
			ind.setElite(false);
			ind.setReproduction(false);
			ind.setMarkedForDeath(false);
		}
		
		int eliteCount = (int)(m_individuals.size() * 0.05);
		for (int i = m_individuals.size() - 1; i > m_individuals.size() - eliteCount - 1; i--) {
			Individual ind = m_individuals.get(i);
			ind.setElite(true);
			ind.setReproduction(true);
		}
		
		//The least fit 5% get the axe
		for (int i = 0; i < eliteCount; i++)
			m_individuals.remove(0);
		
		int breedCount = m_individuals.size() - eliteCount;
		
		for (int i = 0; i < breedCount; i++) {
			Individual individual = m_individuals.get(i);
			double P_b = ((double)i / (double)breedCount);	
			
			if (m_random.nextDouble() <= P_b) {
				individual.setElite(false);
				individual.setReproduction(true);
			}
		}
		
		//All individuals have been chosen for reproduction/removed by now
		if(PHASE_LABELS)
			System.out.print("\t...Complete\n");
	}
	private void reproduction() {
		if (PHASE_LABELS)
			System.out.print("REPRODUCTION");
		
		//First, pick the parent triplets. For this, we'll go Individual by Individual, and if it's marked for reproduction,
		// we add it to a current ParentTriplet. We'll then mark the individual as non-reproducing to prevent double selection
		ArrayList<ParentTriplet> parents = new ArrayList<ParentTriplet>();
		parents.add(new ParentTriplet());	//the starting triplet
		
		ArrayList<Individual> parentPool = new ArrayList<Individual>(m_individuals);	//used a shuffled copy of m_individuals to spread out the love
		Collections.shuffle(parentPool);
		
		for (int i = 0; i < parentPool.size(); i++) {
			Individual individual = parentPool.get(i);
			if (!individual.reproduction())
				continue;	//non-elite and not set for reproduction -- ignore it
			
			//so we want to use this, and we'll check out the latest ParentTriplet in the list
			ParentTriplet workingTriplet = parents.get(parents.size() - 1);
			
			if (workingTriplet.filled()) {	//we need to start a new triplet
				workingTriplet = new ParentTriplet();
				parents.add(workingTriplet);
			}
			
			workingTriplet.add(individual);
		}
		ParentTriplet lastTriplet = parents.get(parents.size() - 1);
		if (!lastTriplet.filled())
			parents.remove(lastTriplet);	//remove the last one, since it's not filled
		
		
		//now, we go through each of the parent triplets and normalize the graphs prior to recombination
		//Note: we need to do this 3 times, to get 3 children, which will replace the parents
		
		for (int i = 0; i < parents.size(); i++) {
			for (int z = 0; z < 3; z++) {
				ParentTriplet triplet = parents.get(i);
				ChromosomePair cp1 = new ChromosomePair();
				ChromosomePair cp2 = new ChromosomePair();
				ChromosomePair cp3 = new ChromosomePair();
				
				cp1.add(triplet.i1.chromosomeOne());
				cp1.add(triplet.i1.chromosomeTwo());
				cp2.add(triplet.i2.chromosomeOne());
				cp2.add(triplet.i2.chromosomeTwo());
				cp3.add(triplet.i3.chromosomeOne());
				cp3.add(triplet.i3.chromosomeTwo());
				
				//Ok so now we have all the parental chromosomes nicely laid out in cp1-3. Now we can begin recombination
				//we will now go gene by gene, and determine if we want to recombine it using P_c, resulting in a list of
				//genes to be recombined.
				
				ArrayList<Gene> toBeRecombined = new ArrayList<Gene>();
				//Regardless of P_c, we're going to choose at LEAST 1 gene. We'll pick it now
				toBeRecombined.add(m_genes.get(randInt(0, m_genes.size() - 1)));
				for (int j = 0; j < m_genes.size(); j++) {
					Gene gene = m_genes.get(j);
					
					if (toBeRecombined.get(0) == gene)
						continue;
					
					if (m_random.nextDouble() <= P_c )
						toBeRecombined.add(gene);
				}

				//toBeRecombined now has the list of genes we'll be recombining, with at least 1 included
				
				//Mark non-elite parents for death at the end of this generation
				if (triplet.i1.elite())
					triplet.i1.setMarkedForDeath(false);
				else
					triplet.i1.setMarkedForDeath(true);
				
				if (triplet.i2.elite())
					triplet.i2.setMarkedForDeath(false);
				else
					triplet.i2.setMarkedForDeath(true);
				
				if (triplet.i3.elite())
					triplet.i3.setMarkedForDeath(false);
				else
					triplet.i3.setMarkedForDeath(true);
				
				
				//So now we have the base and secondary parents picked out. Create the new child
				Individual child = new Individual();
				
				//create the two chromosomes
				Chromosome ch1 = new Chromosome();
				Chromosome ch2 = new Chromosome();
				
				//create the graphs for the 2 new chromosomes, and get a copy of the graph layout from parent 1
				UndirectedGraph<Vertex, DefaultEdge> ch1Graph = Main.deepGraphClone(triplet.i1.chromosomeOne().graph());
				UndirectedGraph<Vertex, DefaultEdge> ch2Graph = Main.deepGraphClone(triplet.i1.chromosomeTwo().graph());
				
				//Give each chromosome its graph
				ch1.setGraph(ch1Graph);
				ch2.setGraph(ch2Graph);
				
				child.setChromosomeOne(ch1);
				child.setChromosomeTwo(ch2);
				
				double totalFitness = triplet.i1.fitness().score + triplet.i2.fitness().score + triplet.i3.fitness().score;
				
				double parent1Probability = triplet.i1.fitness().score / totalFitness;
				double parent2Probability = triplet.i2.fitness().score / totalFitness;
				//parent3 probability is the remainder
				
				double stop1 = 1 - parent1Probability;	//if random # is above this, pick parent 1
				double stop2 = stop1 - parent2Probability; //if  random is between stop1 and stop2, pick parent 2
				//stop3 is 0;
				
				for (int j = 0; j < toBeRecombined.size(); j++) {
					Gene gene = toBeRecombined.get(j);	//the gene we're recombining
				
					Individual mate1;
					Individual mate2;
					double parentChoice1 = m_random.nextDouble();
					if (parentChoice1 >= stop1)
						mate1 = triplet.i1;
					else if (parentChoice1 >= stop2)
						mate1 = triplet.i2;
					else 
						mate1 = triplet.i3;
					
					double parentChoice2 = m_random.nextDouble();
					if (parentChoice2 >= stop1)
						mate2 = triplet.i1;
					else if (parentChoice2 >= stop2)
						mate2 = triplet.i2;
					else 
						mate2 = triplet.i3;
					
					//For each of the vertices in the gene, get the wanted vertex from a parent
					for (int k = 0; k < gene.positions.size(); k++) {	
						int id = gene.positions.get(k);
						Vertex ch1Vertex = vertexById(ch1Graph, id);
						Vertex ch2Vertex = vertexById(ch2Graph, id);
						
						//We base the likelihood of a parent being chosen over another by the parental fitness
						Vertex mate1Vertex = vertexById(mate1.chromosomeOne().graph(), id);
						Vertex mate2Vertex = vertexById(mate2.chromosomeTwo().graph(), id); 		
						
						ch1Vertex.position = mate1Vertex.position;
						ch2Vertex.position = mate2Vertex.position;
					}
				}
				
				child.setElite(false);
				child.setReproduction(false);
				child.setMarkedForDeath(false);
				m_individuals.add(child);
			}
		}
		
		for (int i = 0; i < m_individuals.size(); i++) {
			Individual individual = m_individuals.get(i);
			if (individual.markedForDeath()) {
				m_individuals.remove(i);
				i--;
			}
		}
		
		if (PHASE_LABELS)
			System.out.print("\t...Complete\n");
	}
	private void mutation() {
		if (PHASE_LABELS)
			System.out.print("MUTATION");
		
		for (int i = 0; i < m_individuals.size(); i++) {
			Individual individual = m_individuals.get(i);
			individual.mutated = false;
			
			if (individual.elite())
				continue;
						
			for (int j = 0; j < m_genes.size(); j++) {
				for (int k = 0; k < 2; k++) {	//check both chromosomes
					if (m_random.nextDouble() > P_m || individual.mutated)
						continue;
					
					individual.mutated = true;
					Gene gene = m_genes.get(j);
					
					Mutation m = null;
					double mChance = m_random.nextDouble();
					if (mChance <= 0.33)
						m = Mutation.SHIFT;
					else if (mChance <= 0.66)
						m = Mutation.PIVOT;
					else
						m = Mutation.EDGE;
					
					//So we're mutating this gene in this individual
					Chromosome c = null;
					if (k == 0)
						c = individual.chromosomeOne();
					else
						c = individual.chromosomeTwo();
					
					mutate(c.graph(), gene, m);
				}
			
			}
		}
		
		if (PHASE_LABELS)
			System.out.print("\t...Complete\n");
	}
	private void adaptation() {
		if (PHASE_LABELS)
			System.out.print("ADAPTATION");
	
		if (PHASE_LABELS)
			System.out.print("\t...Complete\n");
	
		if (PRINT_STATS)
			printGenerationalStatistics();
		
		if (PRINT_AVG_FITNESS) {
			//update the average fitness
			double edgeCrossing = 0;
			double energy = 0;
			double alpha = 0;
			double fitness = 0;
			for (int i = 0; i < m_individuals.size(); i++) {
				Individual ind = m_individuals.get(i);
				edgeCrossing += ind.fitness().rawCrossing;
				energy += ind.fitness().rawForce;
				alpha += ind.fitness().rawAlpha;
				fitness += ind.fitness().rawScore;
			}
			edgeCrossing /= m_individuals.size();
			energy /= m_individuals.size();
			alpha /= m_individuals.size();
			fitness /= m_individuals.size();
	
			Fitness f = new Fitness();
			f.rawAlpha = alpha;
			f.rawCrossing = edgeCrossing;
			f.rawForce = energy;
			f.rawScore = fitness;
			
			if (m_fitnessHistory.size() >= FITNESS_HISTORY)	//remove the first to keep the size Ok
				m_fitnessHistory.remove(0);
			
			m_fitnessHistory.add(f);
	
			
			double avgFit = 0;
			for (int i = 0; i < m_fitnessHistory.size(); i++)
				avgFit += m_fitnessHistory.get(i).rawScore;
			avgFit /= m_fitnessHistory.size();
			
			System.out.print(avgFit + "\n");
			
			if (DYNAMIC_MUTATION)
				P_m = mutationProbability();
			//System.out.println(mutationProbability());
		}
		
		if (generation % 25 == 0) {
			for (int i = 0; i < m_individuals.size(); i++) 
				m_individuals.get(i).calculatePhenotype();
		}
		Collections.sort(m_individuals, new IndividualFitnessComparator());
		show(m_individuals.get(m_individuals.size() - 1).phenotype());
			
		
		generation++;
	}
	private void termination() {
		if (PHASE_LABELS)
			System.out.print("TERMINATION");

		Collections.sort(m_individuals, new IndividualFitnessComparator());
		Individual best = m_individuals.get(m_individuals.size() - 1);
		best.calculatePhenotype();
		m_graph = best.phenotype();
		
		if (PHASE_LABELS)
			System.out.print("\t...Complete\n\n");
	}
	

	/*
 	* GENE CALCULATION
 	*/
	private void calculateGenes(UndirectedGraph<Vertex, DefaultEdge> g) {
		//StoerWagnerMinimumCut<Vertex, DefaultEdge> minCut = new StoerWagnerMinimumCut<Vertex, DefaultEdge>(g);
		
		int maxAcceptedSize;
		if (SINGLE_VERTEX_GENES)
			maxAcceptedSize = 1;
		else
			maxAcceptedSize = m_graph.vertexSet().size() / 3;	//maximum acceptable size of a gene
		
		int minAcceptedSize = 1;

		m_genes = recursiveGraphCut(m_graph, minAcceptedSize, maxAcceptedSize);
	}
	private ArrayList<Gene> recursiveGraphCut(UndirectedGraph<Vertex, DefaultEdge> workingGraph, int minSize, int maxSize) {
		ArrayList<Gene> result = new ArrayList<Gene>();
		if (workingGraph.vertexSet().size() <= maxSize || workingGraph.vertexSet().size() <= minSize) {
			Gene gene = new Gene();
			Object[] vertices = workingGraph.vertexSet().toArray();
			for (int i = 0; i < vertices.length; i++) {
				Vertex v = (Vertex) vertices[i];
				gene.positions.add(v.id);
			}
			result.add(gene);
			
			return result;	//empty list
		}
		
		if (workingGraph.vertexSet().size() <= maxSize && m_random.nextBoolean()) {	//in the range of gene sizes? 50% chance of cutting it further
			//copy all the IDs from the remaining graph into a new gene
			Gene gene = new Gene();
			Object[] vertices = workingGraph.vertexSet().toArray();
			for (int i = 0; i < vertices.length; i++) {
				Vertex v = (Vertex) vertices[i];
				gene.positions.add(v.id);
			}
			result.add(gene);
			return result;
		}
		
		//Otherwise, continue recursively cutting the graph segments
		@SuppressWarnings({ "unchecked", "rawtypes" })
		AsWeightedGraph weighted = new AsWeightedGraph(workingGraph, new HashMap<DefaultWeightedEdge, Double>());
		@SuppressWarnings({ "unchecked" })
		StoerWagnerMinimumCut<Vertex, DefaultEdge> minCut = new StoerWagnerMinimumCut<Vertex, DefaultEdge>(weighted);
		
		if (minCut.minCut() == null || minCut.minCut().isEmpty()) {
			
			Object[] vertices = workingGraph.vertexSet().toArray();
			for (int i = 0; i < vertices.length; i++) {
				Gene gene = new Gene();
				gene.positions.add(((Vertex)vertices[i]).id);
				result.add(gene);
			}
			return result;
		}
			
			
		Object[] vertices = workingGraph.vertexSet().toArray();
		ArrayList<Vertex> side1 = new ArrayList<Vertex>(minCut.minCut());
		ArrayList<Vertex> side2 = new ArrayList<Vertex>();
		for (int i = 0; i < vertices.length; i++) {
			Vertex v = (Vertex) vertices[i];
			if (!side1.contains(v))
				side2.add(v);
		}
		//as of now, side1 and side2 contains all the vertices on each part of the cut
		
		//Create a graph for each side of the cut, containing all the vertices/edges in our working graph
		UndirectedGraph<Vertex, DefaultEdge> side1Graph = new SimpleGraph<Vertex, DefaultEdge>(DefaultEdge.class);
		UndirectedGraph<Vertex, DefaultEdge> side2Graph = new SimpleGraph<Vertex, DefaultEdge>(DefaultEdge.class);
		Graphs.addGraph(side1Graph, workingGraph);
		Graphs.addGraph(side2Graph, workingGraph);
		//now go though and remove the vertices that aren't in each side
		
		for (int i = 0; i < side1.size(); i++)	//remove the vertices on side 1 from the side 2 graph
			side2Graph.removeVertex(side1.get(i));
		for (int i = 0; i < side2.size(); i++)	//and remove the vertices from side 2 from the side 1 graph
			side1Graph.removeVertex(side2.get(i));
		
		result.addAll(recursiveGraphCut(side1Graph, minSize, maxSize));
		result.addAll(recursiveGraphCut(side2Graph, minSize, maxSize));
		
		return result;
	}
	
	/*
	 * FITNESS CALCULATION METHODS
	 */
	public int edgeCrossings(Individual individual) {
		if (individual.phenotype() == null)
			individual.calculatePhenotype();
		
		UndirectedGraph<Vertex, DefaultEdge> phenotype = individual.phenotype();
		
		int count = 0;
		Object[] edges = phenotype.edgeSet().toArray();
		for (int i = 0; i < edges.length; i++) {
			DefaultEdge e1 = (DefaultEdge) edges[i];
			for (int j = 0; j < edges.length; j++) {
				if (i == j)
					continue;
				
				DefaultEdge e2 = (DefaultEdge) edges[j];
				//Grab the start/end vertices for each edge
				Vertex e1Source = phenotype.getEdgeSource(e1);
				Vertex e1Target = phenotype.getEdgeTarget(e1);
				Vertex e2Source = phenotype.getEdgeSource(e2);
				Vertex e2Target = phenotype.getEdgeTarget(e2);
				
				if (e1Source == e2Source || e1Source == e2Target || e1Target == e2Source || e1Target == e2Target)
					continue;
				
				Line2D l1 = new Line2D(new Point2D(e1Source.position), new Point2D(e1Target.position));
				Line2D l2 = new Line2D(new Point2D(e2Source.position), new Point2D(e2Target.position));
				if (Line2D.intersects(l1, l2)) {
					count++;
				}
			}
			
		}
		count /= 2;
		return count;
	}
	public double totalVertexEnergy(Individual individual) {
	    /*Here, we follow Kamada & Kawai's method for calculating global spring-based E
	     *
	     * E = sum^{n-1}_{i=1} sum^{n}_{j=i+1} 1/2(k_{ij}) ( |p_i - p_j| - l_{ij})^2
	     * Where: k_{ij} = K / (d_{ij}^2)       where K is a constant, d_{ij} is the shortest-path between v_i and v_j
	     *        l_{ij} = L x d_{ij}           where L is the desireable length of a single edge (i.e. GRID_WIDTH)
	     */
		if (individual.phenotype() == null)
			individual.calculatePhenotype();
		UndirectedGraph<Vertex, DefaultEdge> phenotype = individual.phenotype();
		
		double E = 0;
		
		Object[] vertices = phenotype.vertexSet().toArray();
		for (int i = 0; i < vertices.length - 1; i++) {
			Vertex v1 = (Vertex) vertices[i];
			for (int j = i + 1; j < vertices.length; j++) {


				Vertex v2 = (Vertex) vertices[j];
				double k_ij = k(individual, v1, v2);
				Point p_i = v1.position;
				Point p_j = v2.position;
				double l_ij = l(individual, v1, v2);
				
				Point delta = new Point(); 
				delta.x = p_i.x - p_j.x;
				delta.y = p_i.y - p_j.y;
				double c1 = (0.5) * k_ij;
				double mag = magnitude(delta) - l_ij;
				double magSqr = Math.pow(mag, 2);
				double total = c1 * magSqr;
				E += total;//( (0.5 * k_ij) * Math.pow( (magnitude(delta) - l_ij), 2) );
			}
		}
		return E;
	}
	public double alphaDeviation(Individual individual) {
		int actualAlpha = 0;
		
		if (individual.phenotype().vertexSet() == null)
			individual.calculatePhenotype();
		
		//first, get the extreme-most left, top vertex. We will use this to construct the  grid system, where it starts in the upper-left corner
		Vertex leftMost, topMost, rightMost, bottomMost = null;
		Object[] vertexArray = individual.phenotype().vertexSet().toArray();
		
		topMost = leftMost = rightMost = bottomMost = (Vertex) vertexArray[0];	//initialize with the first element
		for (int i = 1; i < vertexArray.length; i++) {
			Vertex v = (Vertex) vertexArray[i];
			if (v.position.x < leftMost.position.x)
				leftMost = v;
			else if (v.position.x > rightMost.position.x)
				rightMost = v;
			
			if (v.position.y > topMost.position.y)
				topMost = v;
			else if (v.position.y < bottomMost.position.y)
				bottomMost = v;
		}
		
		int left = leftMost.position.x;
		int right = rightMost.position.x + 1;
		int top = topMost.position.y + 1;
		int bottom = bottomMost.position.y;
		
		//ok so now we have the topmost. We can start the grid at the position (leftMost.x, bottomMost.y) and just
		//extend it in the +x and +y direction until we hit the rightmost/bottommost positions
		
		ArrayList<Object> uncheckedVertices = new ArrayList<Object>(Arrays.asList(vertexArray));
		int xGrids = (int) Math.ceil((right - left) / (double)GRID_SIZE);
		int yGrids = (int) Math.ceil((top - bottom) / (double)GRID_SIZE);
		for (int x = 0; x < xGrids; x++) {	//how many squares on the x axis?
			for (int y = 0; y < yGrids; y++) {	//and how many on the y axis?
				boolean mark = false;
				int startX = (GRID_SIZE * x) + left;
				int endX = (startX + GRID_SIZE) ;
				int startY = (GRID_SIZE * y) + bottom ;
				int endY = (startY + GRID_SIZE);
				for (int i = 0; i < uncheckedVertices.size(); i++) {	//check the unchecked vertices
					Vertex v = (Vertex) uncheckedVertices.get(i);

					int pX = v.position.x;
					int pY = v.position.y;
					if (((pX < endX) && pX >= startX) && ((pY < endY) && (pX >= startY))) {
						mark = true;
						uncheckedVertices.remove(i);
					}
				}
				if (mark)	//a vertex exists in this area
					actualAlpha++;
			}
		}
		return Math.abs(actualAlpha - m_alphaIdeal);
	}
	
	
	/*
	 * Force Calculation Methods
	 */
	private double k(Individual individual, Vertex v1, Vertex v2) {
				
		float K = 10;
		FloydWarshallShortestPaths<Vertex, DefaultEdge> sp = new FloydWarshallShortestPaths<Vertex, DefaultEdge>(individual.phenotype());
		double shortestPath = sp.shortestDistance(v1, v2);
		if (Double.isInfinite(shortestPath) || Double.isNaN(shortestPath))
			return 0;
		return K / Math.pow(shortestPath, 2);
	}
	private double l(Individual individual, Vertex v1, Vertex v2) {
			
		UndirectedGraph<Vertex, DefaultEdge> phenotype = individual.phenotype();
		
		FloydWarshallShortestPaths<Vertex, DefaultEdge> sp = new FloydWarshallShortestPaths<Vertex, DefaultEdge>(phenotype);
		double shortestPath = sp.shortestDistance(v1, v2);
		if (Double.isInfinite(shortestPath) || Double.isNaN(shortestPath))
			return 0;
		return GRID_SIZE * shortestPath;
	}
	private double magnitude(Point p) {
		return Math.sqrt((p.x * p.x) + (p.y * p.y));
	}

	/*
	 * MUTATION METHODS
	 */
	public void mutate(UndirectedGraph<Vertex, DefaultEdge> graph, Gene gene, Mutation mutation) {
		//Just do a shift change for now
		int direction = randInt(0, 359);
		int magnitude = randInt(1, GRID_SIZE);	//magnitude ranges from 1 to 2 * square size
		
		for (int i = 0; i < gene.positions.size(); i++) {
			int id = gene.positions.get(i);
			Vertex v = vertexById(graph, id);
			v.position.x += Math.cos(direction) * magnitude;
			v.position.y += Math.sin(direction) * magnitude;
		}
	}
	
	/*
	 * UTIL
	 */
	public int randInt(int aStart, int aEnd) {

		if (aStart > aEnd) {
			throw new IllegalArgumentException("Start cannot exceed End.");
		}
		long range = (long) aEnd - (long) aStart + 1;
		long fraction = (long) (range * m_random.nextDouble());
		int randomNumber = (int) (fraction + aStart);
		return randomNumber;
	}
	public void show(UndirectedGraph<Vertex, DefaultEdge> g) {
		m_main.visualize(g);
		/*System.out.print("Paused (Enter to continue): ");
		try {
			System.in.read();
		} catch (IOException e) {
			e.printStackTrace();
		}*/
	}
	public static Vertex vertexById(UndirectedGraph<Vertex, DefaultEdge> g, int id) {
		Object[] vertices = g.vertexSet().toArray();
		for (int i = 0; i < vertices.length; i++) {
			Vertex v = (Vertex)vertices[i];
			if (v.id == id)
				return v;
		}
		Vertex v = new Vertex();	//if it doesn't exist, return a vertex with id=-1
		v.id = -1;
		return v;
	}

	private void printGenerationalStatistics() {
		for (int i = 0; i < m_individuals.size(); i++)
			m_individuals.get(i).calculatePhenotype();
		
		System.out.print(generation);
		System.out.print(", " + generationalFitness());
		System.out.print(", " + generationalCoherance());
		System.out.print(", " + generationalEdgeCrossings());
		System.out.print(", " + generationalEnergy());
		System.out.print(", " + generationalAlpha() + "\n");
	}
	private double generationalFitness() {
		double avg = 0;
		for (int i = 0; i < m_individuals.size(); i++)
			avg += m_individuals.get(i).fitness().rawScore;
		
		avg /= m_individuals.size();
		return avg;
	}
	private double generationalCoherance() {
		int totalLeft = 0;
		int totalRight = 0;
		int totalBottom = 0;
		int totalTop = 0;
		
		for (int i = 0; i < m_individuals.size(); i++) {
			int left = 0;
			int right = 0;
			int bottom = 0;
			int top = 0;
			
			Individual individual = m_individuals.get(i);

			Object[] vertices = individual.phenotype().vertexSet().toArray();
			left = right = ((Vertex)vertices[0]).position.x;
			top = bottom = ((Vertex)vertices[0]).position.y;
			
			for (int j = 0; j < vertices.length; j++) {
				Vertex v = (Vertex)vertices[j];
				int x = v.position.x;
				int y = v.position.y;
				if (x < left)
					left = x;
				if (x > right)
					right = x;
				if (y > top)
					top = y;
				if (y < bottom)
					bottom = y;
			}
			
			totalLeft += left;
			totalRight += right;
			totalBottom += bottom;
			totalTop += top;
		}
		
		totalLeft /= m_graph.vertexSet().size();
		totalRight /= m_graph.vertexSet().size();
		totalBottom /= m_graph.vertexSet().size();
		totalTop /= m_graph.vertexSet().size();
		
		int width = totalRight - totalLeft;
		int height = totalTop - totalBottom;
		
		int area = width * height;
		return ((double) area);
	}
	private double generationalEdgeCrossings() {
		double total = 0;
		for (int i = 0; i < m_individuals.size(); i++) {
			Individual individual = m_individuals.get(i);
			total += individual.fitness().rawCrossing;
		}
		total /= m_individuals.size();
		return total;
	}
	private double generationalEnergy() {
		double total = 0;
		for (int i = 0; i < m_individuals.size(); i++) {
			Individual individual = m_individuals.get(i);
			total += individual.fitness().rawForce;
		}
		total /= m_individuals.size();
		return total;
		
	}
	private double generationalAlpha() {
		double total = 0;
		for (int i = 0; i < m_individuals.size(); i++) {
			Individual individual = m_individuals.get(i);
			total += individual.fitness().rawAlpha;
		}
		total /= m_individuals.size(); 
		return total;
		
	}
	
	private int expectedGenerationsRemaining() {
		//We need to figure out how many generations are expected to be remaining. We'll take the fitness history
		//to extrapolate. When the fitness has generally stopped decreasing, we'll continue for ~50 generations. So, 
		//we'll keep a 100 generation running history
		return 0;
	}
	private double mutationProbability() {
		/*
		 * We're going to use a sigmoid function that requires the following parameters:
		 * 		*  Expected Population size, when fully converged E
		 * 		*  Maximum mutation probability MAX
		 * 
		 * THis is the equation:
		 *  f(x) = (1 / (1 + e^-((x - E/2)/(E/5)))) * MAX
		 * 
		 */
		
		//for now, assuming E = 200;
		double E = 200;
		
		double exponent = -((generation - (E / 2)) / (E / 5));
		double prob;
		
		
		if (generation % 10 == 0)
			prob = (1 / (1 + Math.pow(Math.E, exponent))) * (MUTATION_CEIL);
		else
			prob = 0.01;	

		return prob;
	}
}