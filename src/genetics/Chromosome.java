package genetics;

import kbarrersi.Vertex;

import org.jgrapht.UndirectedGraph;
import org.jgrapht.graph.DefaultEdge;

public class Chromosome {

	private UndirectedGraph<Vertex, DefaultEdge> m_graph;
	
	public Chromosome() {
	}

	
	
	
	
	public UndirectedGraph<Vertex, DefaultEdge> graph() { return m_graph; }
	public void setGraph(UndirectedGraph<Vertex, DefaultEdge> m_graph) { this.m_graph = m_graph; }

}
