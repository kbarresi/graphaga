package kbarrersi;

import java.awt.Dimension;
import java.awt.Point;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Hashtable;

import javax.swing.JApplet;
import javax.swing.JFrame;

import org.jgrapht.UndirectedGraph;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.SimpleGraph;

import com.mxgraph.swing.mxGraphComponent;
import com.mxgraph.util.mxConstants;
import com.mxgraph.util.mxPoint;
import com.mxgraph.view.mxGraph;
import com.mxgraph.view.mxStylesheet;

import genetics.*;


public class Main extends JApplet {

	private static final long serialVersionUID = -1966100738547337177L;
	private static final Dimension DEFAULT_SIZE = new Dimension(800, 800);
		
	public static void main(String[] args) {
		System.out.println("Begining the simulation");
		if (args.length != 1) {
			System.out.println("No graph file supplied!");
			System.exit(1);
		}
		
		Main applet = new Main();
		applet.init();
		
		JFrame frame = new JFrame();
		frame.getContentPane().add(applet);
		frame.setTitle("AGA");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.pack();
		frame.setSize(DEFAULT_SIZE);
		frame.setVisible(true);
		
		applet.startAGA();
		//applet.testGraphFitness();
		
		System.out.println("Finished simulation");
		//System.exit(0);
	}

	private mxGraph graph;
	
	public void init() {
		graph = new mxGraph();
		graph.setEdgeLabelsMovable(false);
		graph.setCellsMovable(true);
		graph.setCellsResizable(false);
		graph.setGridEnabled(true);
		graph.setCellsDisconnectable(false);
		graph.setOrigin(new mxPoint(0, 0));
		graph.setAllowDanglingEdges(false);

		//Set the styles
		mxStylesheet stylesheet = graph.getStylesheet();
		Hashtable<String, Object> style = new Hashtable<String, Object>();
		style.put(mxConstants.STYLE_SHAPE, mxConstants.SHAPE_ELLIPSE);
		style.put(mxConstants.STYLE_OPACITY, 50);
		style.put(mxConstants.STYLE_FONTCOLOR, "#774400");
		stylesheet.putCellStyle("ROUNDED", style);
		
		mxGraphComponent graphComponent = new mxGraphComponent(graph);
		getContentPane().add(graphComponent);
	}
	
	public void startAGA() {
		String file = "D:/Documents/graph.csv";
		//String file = "/home/kbarresi/graph.csv";
		UndirectedGraph<Vertex, DefaultEdge> inputGraph = loadGraphFromFile(file);
		Population p = new Population(inputGraph, this);
		
		
		UndirectedGraph<Vertex, DefaultEdge> result  = p.run();

		clear();
		Object[] vertices = result.vertexSet().toArray();
		int left = 0;
		int bottom = 0;
		left = ((Vertex)vertices[0]).position.x;
		bottom = ((Vertex)vertices[0]).position.y;
		
		for (int i = 1; i < vertices.length; i++) {
			Vertex v = (Vertex) vertices[i];
			int x = v.position.x;
			int y = v.position.y;
			
			if (left > x)
				left = x;
			if (bottom > y)
				bottom = y;
		}		
		
		HashMap<Vertex, Object> vertexMap = new HashMap<Vertex, Object>();
		HashMap<DefaultEdge, Object> edgeMap = new HashMap<DefaultEdge, Object>();
		
		Object parent = graph.getDefaultParent();
		graph.getModel().beginUpdate();
		
		try {
			for (Vertex v : result.vertexSet()) {
				System.out.println("Vertex(" + v.id + "):\t" + (v.position.x - left + 25) + ", " + (v.position.y - bottom + 25));
				Object w = graph.insertVertex(parent, null, String.valueOf(v.id), (double)(v.position.x - left + 25), (double)(v.position.y - bottom + 25), (double)25, (double)25, "ROUNDED");
				
				graph.updateCellSize(w);
				vertexMap.put(v, w);
			}
			for (DefaultEdge e : result.edgeSet()) {
				Object e2 = graph.insertEdge(parent,  null, "", vertexMap.get(result.getEdgeSource(e)), vertexMap.get(result.getEdgeTarget(e)), "startArrow=none;endArrow=none;");
				edgeMap.put(e,  e2);
			}
		} finally {
			graph.getModel().endUpdate();
		}
	}
	
	public void visualize(UndirectedGraph<Vertex, DefaultEdge> g) {
		clear();
		Object[] vertices = g.vertexSet().toArray();
		int left = 0;
		int bottom = 0;
		left = ((Vertex)vertices[0]).position.x;
		bottom = ((Vertex)vertices[0]).position.y;
		
		for (int i = 1; i < vertices.length; i++) {
			Vertex v = (Vertex) vertices[i];
			int x = v.position.x;
			int y = v.position.y;
			
			if (left > x)
				left = x;
			if (bottom > y)
				bottom = y;
		}
		
		
		Object parent = graph.getDefaultParent();
		graph.getModel().beginUpdate();
		
		HashMap<Vertex, Object> vertexMap = new HashMap<Vertex, Object>();
		HashMap<DefaultEdge, Object> edgeMap = new HashMap<DefaultEdge, Object>();
		try {
			for (Vertex v : g.vertexSet()) {
				System.out.println(v.id + ", " + (v.position.x - left + 25) + ", " + (v.position.y - bottom + 25));
				Object w = graph.insertVertex(parent, null, String.valueOf(v.id), (double)(v.position.x - left + 25), (double)(v.position.y - bottom + 25), (double)25, (double)25, "ROUNDED");
				vertexMap.put(v, w);
			}
			for (DefaultEdge e : g.edgeSet()) {
				Object e2 = graph.insertEdge(parent,  null, "", vertexMap.get(g.getEdgeSource(e)), vertexMap.get(g.getEdgeTarget(e)), "startArrow=none;endArrow=none;");
				edgeMap.put(e,  e2);
			}
		} finally {
			graph.getModel().endUpdate();
		}
	}
	private void clear() {
		graph.removeCells(graph.getChildVertices(graph.getDefaultParent()));
	}
	
	private static UndirectedGraph<Vertex, DefaultEdge> loadGraphFromFile(String file) {
		UndirectedGraph<Vertex, DefaultEdge> g = new SimpleGraph<Vertex, DefaultEdge>(DefaultEdge.class);
		
		//read the file and get the vertex pairs
		System.out.println("Loading graph from file: " + file);
		BufferedReader br = null;
		String line = "";
		String splitBy = ",";
		
		try {
			br = new BufferedReader(new FileReader(file));
			while ((line = br.readLine()) != null) {
				System.out.println("\tLine: " + line);
				String[] values = line.split(splitBy);
				
				if (values.length < 2)
					continue;
				
				String v1Str = values[0].trim();
				String v2Str = values[1].trim();
				
				Vertex v1 = new Vertex();
				v1.id = Integer.parseInt(v1Str);
				v1.position = new Point(0, 0);
				
				Vertex v2 = new Vertex();
				v2.id = Integer.parseInt(v2Str);
				v2.position = new Point(0, 0);
				
				Object[] vertices = g.vertexSet().toArray();
				if (vertices.length == 0) {
					g.addVertex(v1);
					g.addVertex(v2);
				} else {
					boolean addV1 = true;
					boolean addV2 = true;
					for (int i = 0; i < vertices.length; i++) {
						Vertex v = (Vertex) vertices[i];
						
						if (v.id == v1.id) {
							v1 = v;
							addV1 = false;
						}
						if (v.id == v2.id) {
							v2 = v;	
							addV2 = false;
						}
					}
					if (addV1)
						g.addVertex(v1);
					if (addV2)
						g.addVertex(v2);
				}
				
				g.addEdge(v1, v2);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		
		System.out.print("Finished loading graph from file:\t");
		Object[] obj = g.vertexSet().toArray();
		System.out.print("Vertices: " + obj.length + "\tEdges: " + g.edgeSet().toArray().length + "\n");
		return g;
	}
	
	/*
	 * TEST FUNCTIONS
	 */
	private void testGraphFitness() {
		String file = "D:/Documents/graph.csv";
		//String file = "/home/kbarresi/graph.csv";
		UndirectedGraph<Vertex, DefaultEdge> inputGraph = loadGraphFromFile(file);
		Population p = new Population(inputGraph, this);
		
		int bound = (int) (Math.sqrt(inputGraph.vertexSet().size()) * 2 * Population.VERTEX_WIDTH);
		
		UndirectedGraph<Vertex, DefaultEdge> ch1Graph = new SimpleGraph<Vertex, DefaultEdge>(DefaultEdge.class);
		UndirectedGraph<Vertex, DefaultEdge> ch2Graph = new SimpleGraph<Vertex, DefaultEdge>(DefaultEdge.class);
		
		Individual individual = new Individual();
		
		Object[] edgeArray = inputGraph.edgeSet().toArray();
		for (int j = 0; j < edgeArray.length; j++) {
			DefaultEdge e = (DefaultEdge) edgeArray[j];
			Vertex v1 = inputGraph.getEdgeTarget(e);
			Vertex v2 = inputGraph.getEdgeSource(e);
			
			//we need to create an edge connecting the vertices with the ids of v1 and v2;
			
			//first, get the vertices in chromosome 1's graph that matches the ids of v1 and v2
			Vertex newV1Ch1 = Population.vertexById(ch1Graph, v1.id);
			Vertex newV2Ch1 = Population.vertexById(ch1Graph, v2.id);
			
			//and do the same for the vertices in chromosome 2's graph
			Vertex newV1Ch2 = Population.vertexById(ch2Graph, v1.id);
			Vertex newV2Ch2 = Population.vertexById(ch2Graph, v2.id);
			
			//if v1's compliment in chromosome 1 doesn't exist, create and add it
			if (newV1Ch1.id == -1) {
				newV1Ch1.id = v1.id;	//give it the ID of v1
				newV1Ch1.position.x = p.randInt(-bound, bound);	//and a random position
				newV1Ch1.position.y = p.randInt(-bound, bound);
				ch1Graph.addVertex(newV1Ch1);	//now add it to Chromosome 1's graph
			}
			
			if (newV2Ch1.id == -1) {	//same for v2's compliment
				newV2Ch1.id = v2.id;
				newV2Ch1.position.x = p.randInt(-bound, bound);
				newV2Ch1.position.y = p.randInt(-bound, bound);
				ch1Graph.addVertex(newV2Ch1);
			}
			
			//Now check for chromosome 2's stuff. We want their positions to be the same as those in Chromosome 1
			if (newV1Ch2.id == -1) {
				newV1Ch2.id = v1.id;
				newV1Ch2.position.x = p.randInt(-bound, bound);
				newV1Ch2.position.y = p.randInt(-bound, bound);
				ch2Graph.addVertex(newV1Ch2);
			}
			
			if (newV2Ch2.id == -1) {
				newV2Ch2.id = v2.id;
				newV2Ch2.position.x = p.randInt(-bound, bound);
				newV2Ch2.position.y = p.randInt(-bound, bound);
				ch2Graph.addVertex(newV2Ch2);
			}
			
			ch1Graph.addEdge(newV1Ch1, newV2Ch1);
			ch2Graph.addEdge(newV1Ch2, newV2Ch2);
		}
		
		Chromosome ch1 = new Chromosome();
		ch1.setGraph(ch1Graph);
		
		Chromosome ch2 = new Chromosome();
		ch2.setGraph(ch2Graph);
		
		individual.setChromosomeOne(ch1);
		individual.setChromosomeTwo(ch2);
	
		individual.calculatePhenotype();
		
		int edgeCrossings = p.edgeCrossings(individual);
		double force = p.totalVertexEnergy(individual);
		double aDev = p.alphaDeviation(individual);
		
		System.out.println("FITNESS TEST: (" + edgeCrossings + ")\t(" + force + ")\t(" + aDev + ")");
		visualize(individual.phenotype());
	}
	
	public static UndirectedGraph<Vertex, DefaultEdge> deepGraphClone(UndirectedGraph<Vertex, DefaultEdge> g) {
		UndirectedGraph<Vertex, DefaultEdge> copy = new SimpleGraph<Vertex, DefaultEdge>(DefaultEdge.class);

		HashMap<Vertex, Vertex> vertexMap = new HashMap<Vertex, Vertex>();
		HashMap<DefaultEdge, DefaultEdge> edgeMap = new HashMap<DefaultEdge, DefaultEdge>();

		for (Vertex v : g.vertexSet()) {
			Vertex copyVertex = new Vertex();
			copyVertex.id = v.id;
			copyVertex.position.x = v.position.x;
			copyVertex.position.y = v.position.y;

			vertexMap.put(v, copyVertex);
			copy.addVertex(copyVertex);
		}
		for (DefaultEdge e : g.edgeSet()) {
			Vertex origV1 = g.getEdgeSource(e);
			Vertex origV2 = g.getEdgeTarget(e);

			Vertex copyV1 = vertexMap.get(origV1);
			Vertex copyV2 = vertexMap.get(origV2);

			DefaultEdge copyEdge = copy.addEdge(copyV1, copyV2);
			edgeMap.put(e, copyEdge);
		}

		return copy;
	}
}
