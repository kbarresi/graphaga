package util;

import genetics.Individual;
import java.util.Comparator;

public class IndividualFitnessComparator implements Comparator<Individual> {
	@Override
    public int compare(Individual i1, Individual i2) {
		if (i1.fitness().score > i2.fitness().score)
			return 1;
		else if (i1.fitness().score < i2.fitness().score)
			return -1;
		else
			return 0;
    }
}